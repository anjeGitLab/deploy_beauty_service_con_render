<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

    <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
      <html>

      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <jsp:useBean id="usuariGestor" class="beauty.service.service.repositori.Entitats.Usuari" scope="session">
        </jsp:useBean>
        <title>Registre Gestor de Centre</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/registreOK.css">
        <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
        <script src="/js/validacioFormularis.js" type="text/javascript"></script>
        <link rel="stylesheet" href="/css/validacioFormularis.css">
        <link rel="stylesheet" href="/css/consentimentBanner.css">
        <script src="/js/cookies.js" type="text/javascript"></script>
        <script src="/js/consentimentBanner.js" type="text/javascript"></script>

      </head>

      <body>

        <div class="menu-btn">
          <i class="fas fa-bars fa-2x"></i>
        </div>

        <div class="container">
          <!-- Header -->
          <header id="home-header">
            <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

            <nav class="main-nav">
              <ul class="main-menu">
                <li><a href="/">Home</a></li>
                <li><a href="quiSom">Qui Som</a></li>
                <li><a href="buscarReserva">Reservar tractament</a></li>
              </ul>
              <ul class="right-menu">
                <li><a href="login">Login</a></li>
                <li><a href="tipusUsuari">Registre</a></li>
              </ul>
            </nav>
          </header>

          <!--Main-->

          <div class="container-main">
            <h3>Registre correcte!</h3>
            <img src="/img/RegistreOK.jpg" alt="Moltes Gràcies!">
            <div class="button">
              <a href="login" class="btn">Iniciar sessió</a>
            </div>
          </div>

          <!--Footer-->
          <footer id="main-footer" class="footer">
            <div class="footer-inner">
              <div><img src="/img/Dark-logo.png" class="logo"></div>
              <ul>
                <li><a href="#">Contacte</a></li>
                <li><a href="#">Politica de la Web</a></li>
                <li><a href="#">&copy; Beauty Service 2022</a></li>
              </ul>
            </div>

          </footer>

        </div>

      </body>

      </html>