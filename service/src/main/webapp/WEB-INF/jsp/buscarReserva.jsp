<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

     <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

            <html>

            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
                <link rel="stylesheet" href="/css/style.css">
                <link rel="stylesheet" href="/css/buscarReserva.css">
                <script src="/js/home.js" type="text/javascript"></script>
                <link rel="stylesheet" href="/css/consentimentBanner.css">
                <script src="/js/cookies.js" type="text/javascript"></script>
                <script src="/js/consentimentBanner.js" type="text/javascript"></script> 
                <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
                <script src="https://kit.fontawesome.com/7c1b1fcd24.js" crossorigin="anonymous"></script>
                <title>Reserva el teu tractament</title>
            </head>

            <body>

                <div class="menu-btn">
                    <i class="fas fa-bars fa-2x"></i>
                </div>

                <div class="container">
                    <!-- Header -->
                    <header id="home-header">
                        <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

                        <nav class="main-nav">
                            <ul class="main-menu">
                                <li><a href="/">Home</a></li>
                                <li><a href="#">Qui Som</a></li>
                                <li><a href="buscarReserva">Reservar tractament</a></li>
                            </ul>
                            <ul class="right-menu">
                                <li><a href="benvingut">${logat}</a></li>
                                <li><a href="login">${login}</a></li>
                                <li><a href="tipusUsuari">${registre}</a></li>
                                <li><a href="logout">${tancarSessio}</a></li>
                            </ul>
                        </nav>
                    </header>

                    <!--Main-->
                    <main>

                        <div class="container-main">
                            
                            <div class="formulari">
                                <h1>Reserva el teu tractament</h1>
                                <form id="buscarReserva" action="/cercaReserva" method="get">
                                    <div>
                                        <label id="tipusServeis">Selecciona el tipus de servei:</label>
                                        <select name="tipusServeis">
                                            <c:forEach items="${tipusServeis}" var="tipusServeis">
                                                <option id="tipusServeis" name="tipusServeis"
                                                    value="${tipusServeis.nomTipusServei}" ${tipusServeis.nomTipusServei == nomTipusServei ? 'selected="selected"' : ''}>${tipusServeis.nomTipusServei}
                                                    </option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div>
                                        <label id="codiPostal">Codi postal:</label>
                                        <input type="text" name="codiPostal" path="codiPostal" value="${codiPostal}" />
                                    </div>
                                    <div>
                                        <button name="cercar" value="cercar" class="btn">Cercar</button>
                                        ${capSalo}
                                    </div>
                                </form>
                            </div>
                            <div class="reservar">
                                <img src="/img/reservar_tractament.jpg" alt="Reservar">
                            </div>

                        </div>
                    </main>

                    <!--Footer-->
                    <footer id="main-footer" class="footer">
                        <div class="footer-inner">
                            <div><img src="/img/Dark-logo.png" class="logo"></div>
                            <ul>
                                <li><a href="#">Contacte</a></li>
                                <li><a href="#">Politica de la Web</a></li>
                                <li><a href="#">&copy; Beauty Service 2022</a></li>
                            </ul>
                        </div>

                    </footer>
                </div>

            </body>

            </html>