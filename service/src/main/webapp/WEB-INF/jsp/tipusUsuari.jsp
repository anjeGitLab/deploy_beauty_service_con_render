<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <!DOCTYPE html>
    <html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/tipusUsuari.css">
        <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>        
        <link rel="stylesheet" href="/css/consentimentBanner.css">
        <script src="/js/cookies.js" type="text/javascript"></script>
        <script src="/js/consentimentBanner.js" type="text/javascript"></script>        
        <title>Formulari de registre</title>
    </head>

    <body>

        <div class="menu-btn">
            <i class="fas fa-bars fa-2x"></i>
        </div>

        <div class="container">
            <!-- Header -->
            <header id="home-header">
                <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

                <nav class="main-nav">
                    <ul class="main-menu">
                        <li><a href="/">Home</a></li>
                        <li><a href="quiSom">Qui Som</a></li>
                        <li><a href="buscarReserva">Reservar tractament</a></li>
                    </ul>
                    <ul class="right-menu">
                        <li><a href="login">Login</a></li>
                        <li><a href="tipusUsuari">Registre</a></li>
                    </ul>
                </nav>
            </header>

            <!--Main-->
            <main>

                <div class="container-main">
                    <div class="title">
                        <h1>Indica si ets un client o un saló </h1>
                    </div>

                    <section class="usuari-cards">
                        <div>
                            <img src="/img/Client.jpg" alt="Client">
                            <div class="card-content">
                                <a href="showRegistreClient" class="btn">Soc un client</a>
                            </div>
                        </div>
                        <div>
                            <img src="/img/Salo.jpg" alt="Saló">
                            <div class="card-content">
                                <a href="showRegistreGestor" class="btn">Soc un salo</a>
                            </div>
                        </div>
                    </section>

                </div>

            </main>

                    <!--seccio consentiment de banner-->
                    <div class="cookie-banner">
                        <div class="cookie-close accept-cookie"></div>
                        <div class="container">
                            <p>Utilitzem cookies pr&#242;pies per a un &#250;s t&#233;cnic i per a un bon funcionament dels nostres serveis.
                                <br>Si accepteu o continueu navegant, considerem que accepteu les condicions.<br>
                                <a href="https://www.boe.es/buscar/pdf/2018/BOE-A-2018-16673-consolidado.pdf"> Per m&#233;s infomaci&#243;
                                    sobre la protecci&#243; de dades</a>
                            </p>
                            <button type="button" class="btn acceptar-cookie">Aceptar</button>
                        </div>
                    </div>

            <!--Footer-->
            <footer id="main-footer" class="footer">
                <div class="footer-inner">
                    <div><img src="/img/Dark-logo.png" class="logo"></div>
                    <ul>
                        <li><a href="#">Contacte</a></li>
                        <li><a href="#">Politica de la Web</a></li>
                        <li><a href="#">&copy; Beauty Service 2022</a></li>
                    </ul>
                </div>

            </footer>

        </div>
    </body>

    </html>