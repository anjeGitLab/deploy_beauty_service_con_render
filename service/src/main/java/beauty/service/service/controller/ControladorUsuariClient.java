package beauty.service.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import beauty.service.service.service.UsuariService;
import beauty.service.service.repositori.Entitats.Usuari;

/**
 * Classe que representa el controlador que gestiona els usuaris de tipus gestor
 */
@Controller
public class ControladorUsuariClient {
    
    @Autowired
    private UsuariService service;

    /***
     * Mètode que dóna d'alta un usuari a la base de dades completant les dades en un formulari, 
     * mitjançant l'anotació @ModelAttribute. Si ja existeix el e-mail o el nom d'usuari, no deixa
     * registrar-se i torna de nou el formulari amb un missatge d'error
     * @param usuariClient Valor de tipus {@link Usuari} "CLIENT" que es passa per paràmetre, per poder obtenir el mail i el nom d'usuari
     * @param model Es passa un Model de Spring MVC a retornar.
     * @return vista 'registreOK'
     */
    @RequestMapping(path="/registreClientProcess", method= RequestMethod.POST)
    public String addNewUsuariClient(@ModelAttribute("usuariClient") Usuari usuariClient, ModelMap model) {
        
        Usuari mailUsuariExistent = service.getUsuariByEmail(usuariClient.getMail());
        Usuari nomUsuariExistent = service.getUsuariByUsername(usuariClient.getNomUsuari());
            
        if (mailUsuariExistent == null && nomUsuariExistent == null) {

            service.guardaUsuari(usuariClient);
                
            model.addAttribute("nomUsuari", usuariClient.getNomUsuari());
            model.addAttribute("nom", usuariClient.getNomUsuari());
            model.addAttribute("mail", usuariClient.getMail());

            return "registreOK";
        }
        if (mailUsuariExistent != null) {
            model.addAttribute("missatgeMail", "Ja existeix un usuari amb aquest mail");
        }
        if (nomUsuariExistent != null) {
            model.addAttribute("missatgeNomUsuari", "Nom d'usuari no disponible");
        }
                 
        return "registreClient";
        
    }


}
