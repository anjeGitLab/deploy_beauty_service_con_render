package beauty.service.service.controller;

import java.security.Principal;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import beauty.service.service.repositori.Entitats.Login;
import beauty.service.service.repositori.Entitats.Salo;
import beauty.service.service.repositori.Entitats.TipusServeis;
import beauty.service.service.repositori.Entitats.Usuari;
import beauty.service.service.service.TipusServeisService;
import beauty.service.service.service.UsuariService;

/**
 * <h1>Es controla les vistes principals</h1>
 * <p>
 * La següent classe, és controller principal per administrar el fluxe
 * de dades entre el Model i les Vistes
 * <p>
 */
@Controller
public class ControladorHome {

    @Autowired
    private TipusServeisService tipusServeisService;

    @Autowired
    private UsuariService usuariService;

    /**
     * <p>
     *  Aquest mapping controller, amb una petició get i una url buida, ens
     *  retorna una vista home
     *  @param model     El model a retornar en aquest cas en home
     *  @param principal L'usari actual
     * 
     * @return String Retorna el nom de la vista, home
     *              
     */

    @GetMapping(path = "/")
    public String home(ModelMap model, Principal principal) {
        if (principal != null) {
            Usuari usuariLogat = usuariService.getUsuariByUsername(principal.getName());
            String missatgeLogat = "Hola " + usuariLogat.getNomUsuari();
            model.addAttribute("tancarSessio", "Tancar sessió");
            model.addAttribute("logat", missatgeLogat);
        } else {
            model.addAttribute("login", "Login");
            model.addAttribute("registre", "Registre");
        }

        return "home";
    }

    /**
     * <p>
     * Aquest mapping controller, treballa amb una petició get i una url "/c", i es
     * fa servir para treballar un tipus d'usuari
     * del projecte ens retorna una vista de nom "c"
     * </p>
     * 
     * @param map Es treballa amb un array map
     * @return String Ens retorn el tipus usuari
     */
    @GetMapping(path = "/tipusUsuari")
    public String tipusUsuari(Map<String, Object> map) {
        return "tipusUsuari";
    }

    /**
     * <p>
     *  Aquest mapping controller, gestiona el registre d'un usuariGestor. Fa
     * servir una petició get i una url
     * "/showRegistreGestor", ens retorna una vista de nom "showRegistreGestor",
     * passant-li com atribut un objecte tipus Usuari.
     * </p>
     * 
     * @param model objecte model de la vista
     * @return String En retorna una vista de nom "registreGestorCentre"
     */
    @GetMapping(path = "/showRegistreGestor")
    public String showRegistreGestor(Model model) {
        model.addAttribute("usuariGestor", new Usuari());
        return "registreGestorCentre";
    }

    /**
     * <p>
     * Aquest mapping controller, fa us d'una petició get i una url
     * "/showRegistreClient", ens retorna una
     * vista de nom "registreClient", passant-li com atribut un objetec tipus Usuari
     * </p>
     *
     * @param model El model de la vista mostrar
     * @return String Retorna una vista de nom
     *         {@link #showRegistreClient(Model)}
     */

    @GetMapping(path = "/showRegistreClient")
    public String showRegistreClient(Model model) {
        model.addAttribute("usuariClient", new Usuari());
        return "registreClient";
    }

    /***
     * Mètode que retorna la jsp per registrar un saló de bellesa
     * 
     * @param model El objecte tipus model de la vista
     * @return vista "nouSalo"
     */
    @GetMapping(path = "/showRegistreSalo")
    public String showRegistreSalo(Model model) {
        model.addAttribute("salo", new Salo());
        List<TipusServeis> tipusServeis = tipusServeisService.findAllListTipusServeis();
        model.addAttribute("serveis", tipusServeis);
        return "nouSalo";
    }

    /**
     * 
     * <p>
     * Aquest mapping controller, administra la vista login d'un usuari o client,amb
     * una
     * petició get a una url "/login", ens retorna una vista de nom "login", es
     * passa un atribut de un
     * objecte tipus Login
     * </p>
     *
     * @param model Objecte model de la vista
     * @return String nom de la vista, on l'usuari podrà entrar al sistema
     */
    @GetMapping("/login")
    public String showLogin(ModelMap model) {
        model.addAttribute(new Login());

        return "login";
    }

    /**
     * <p>
     * Aquest mapping controller, gestiona la vista logout d'un usuari o client amb
     * una petició
     * get petición i una url "/logout",
     * ens retorna una vista de nom "logout"
     * </p>
     * 
     * @return String Nom de la vista, on l'usuari podrà sortir del sistema
     */
    @GetMapping("/logout")
    public String logout() {
        return "logout";
    }

    /***
     * Mètode que retorna una vista que confirma el correcte registre per part de
     * l'usuari
     * 
     * @param model Object model que gestiona la vista
     * @return vista Retorna una vista de nom "registreOK"
     */
    @GetMapping("/registreOK")
    public String registreOK(ModelMap model) {

        return "registreOK";
    }

    /**
     * Mètode que retorna una vista per mostrar al mon qui som com a pàgina web
     * 
     * @param model Object model que gestiona la vista.
     * @return vista Retorna una vista de nom "quiSom".
     */
    @GetMapping("/quiSom")
    public String quiSom(ModelMap model) {

        return "quiSom";
    }

}