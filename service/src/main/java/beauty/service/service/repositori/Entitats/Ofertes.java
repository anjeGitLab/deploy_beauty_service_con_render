package beauty.service.service.repositori.Entitats;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * <h1>Classe que representa una entitat de tipus Ofertes</h1>
 * <p>
 * Aquesta entitat gestionarà les ofertes disponibles per un client.
 * A més aquesta classe té una relació Molts a Un amb la taula {@link #salo} amb
 * nif_salo, també amb la
 * classe {@link Serveis}, amb la clau forana {@link #idServicio}
 * </p>
 * 
 */
@Entity
@Table(name = "Ofertes")
public class Ofertes implements Serializable {

    private static final long serialVersionUID = 1L;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "idReserva")
    private Reserves reserva;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private Salo salo;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "idServei")
    private Serveis serveis;

    @Id
    @Column(name = "idOferta")
    @Size(max = 11)
    @NotNull
    private int idOferta;

    @NotNull
    @Column(name = "nifSalo")
    @Size(max = 45)
    private String nifSalo;

    @NotNull
    @Column(name = "idServicio")
    @Size(max = 11)
    private int idServicio;

    @Column(name = "descripcio")
    @Size(max = 120)
    private String descripcio;

    @NotNull
    @Column(name = "data")
    private Date data;

    @NotNull
    @Column(name = "tramHorari")
    @Size(max = 45)
    private String tramHorari;

    @NotNull
    @Column(name = "preu")
    private double preu;

    /**
     *   * Constructor per obtenir instancies de tipus Ofertes
     *      * @param nifSalo Clau forana tipus String, per establer la racion Molts
     * a Un amb {@link Salo}
     *      * @param idServicio Clau que pertanya a la taula {@link Servies}
     *      * @param descripcio És una petita descripció de cada oferta
     *      * @param data La data de cada oferta
     *      * @param tramHorari S'estableix  un rang de data
     *      * @param preu La quantitat a cobrar en cada oferta
     */

    public Ofertes(String nifSalo, int idServicio, String descripcio, Date data, String tramHorari, double preu) {
        this.nifSalo = nifSalo;
        this.idServicio = idServicio;
        this.descripcio = descripcio;
        this.data = data;
        this.tramHorari = tramHorari;
        this.preu = preu;
    }

    /**
     * Constructor buit {@link Ofertes} , que s'utilitzarà en la creació d'un
     * objecte que serà l'instància d'una classe. Generalment
     * duu a terme les operacions requerides per inicialitzar la classe abans que
     * els mètodes siguin invocats
     * o s'accedeixi als metòdes
     */
    public Ofertes() {
    }

    /**
     * Funció de relación etre {@link Ofertes} i {@link Reserves}, desde ofertes
     * s'obté un objecte de tipus {@link Reserves}
     * 
     * @return Reserves Ens retorna un objecte de tipus {@link Reserves}
     */
    public Reserves getReserva() {
        return this.reserva;
    }

    /**
     *      * Dins la relació de {@link Ofertes} i {@link Reserves}, s'actualitza un
     * objecte de tipus Reserves
     *      * @param reserva El objecte a actualitzar.
     */
    public void setReserva(Reserves reserva) {
        this.reserva = reserva;
    }

    /**
     * Getter dins la relació entre {@link Ofertes} i {@link Salo}. S'obté un
     * objecte de tipus {@link Salo}
     * 
     * @return Salo Objecte de tipus {@link Salo} a obtenir.
     */
    public Salo getSalo() {
        return this.salo;
    }

    /**
     * Funció per guardar o actualitzar un objecte tipus {@link Salo}
     * passant-li per paràmetre un objecte del mateix tipus
     */
    public void setSalo(Salo salo) {
        this.salo = salo;
    }

    /**
     *      * Fent referencia a la relació entre {@link Ofertes} i {@link Serveis},
     * s'obté des de {@link Ofertes} s'obté un objete
     *      * de tipus {@link Serveis}
     *      * @return Serveis Rebem un objecte de tipus {@link Serveis}
     */
    public Serveis getServeis() {
        return this.serveis;
    }

    /**
     * Continuant amb la relació entre {@link Ofertes} i {@link Serveis},
     * s'actualitza un objecte {@link Serveis}
     * 
     * @param serveis Amb aquest valor de tipus {@link Serveis} es canvia el valor
     *                del mateix tipus.
     */
    public void setServeis(Serveis serveis) {
        this.serveis = serveis;
    }

    /**
     * S'obté la propietat d'un atribut de {@link #idOferta}
     * 
     * @return int Ens retorna un enter, que serà el valor del id demanat.
     */
    public int getIdOferta() {
        return idOferta;
    }

    /**
     * Tot i que no es gaire recomanable, i únicament en casos especials, aquesta
     * funció canvia el
     * valor del {@link #idOferta}
     * 
     * @param idOferta Representa el id a actualitzar
     */
    public void setIdOferta(int idOferta) {
        this.idOferta = idOferta;
    }

    /**
     * En la relació entre l'entitat {@link Ofertes} i {@link Salo} s'obté el
     * {@link #nifSalo}
     * 
     * @return String Com valor de tipus String, s'obté el {@link #nifSalo}
     */
    public String getNifSalo() {
        return nifSalo;
    }

    /**
     * Continuant amb la relació de Molst a Un amb la taula {@link Salo}, des de
     * {@link Ofertes}
     * es canvia el valor de {@link #nifSalo}
     * 
     * @param nifSalo Amb aquest paràmetre s'actualitza el valor de mateix tipus
     */
    public void setNifSalo(String nifSalo) {
        this.nifSalo = nifSalo;
    }

    /**
     * Getter que obté el valor de propietat {@link #idServicio} dins la relació
     * Molts a Un de la taula {@link Serveis}
     * 
     * @return int Ens retorna l'ide de {@link #idServicio} con un valor enter
     */
    public int getIdServicio() {
        return idServicio;
    }

    /**
     * S'actualitza el valor de {@link #idServicio} de la taula {@link Serveis}
     * 
     * @param idServicio Valor a canviar de tipus enter
     */
    public void setIdServicio(int idServicio) {
        this.idServicio = idServicio;
    }

    /**
     * Funció per obtenir el valor de l'atribut {@link #descripcio}
     * 
     * @return String Ens retorna un valor de tipus String de l'atribut
     *         {@link #descripcio}
     */
    public String getDescripcio() {
        return descripcio;
    }

    /**
     * Setter per actualitzar el valor de {@link #descripcio} passant-li per
     * paràmetre un valor tipus String
     *      * @param descripció Valor per actualitzar l'atribut {@link #descripcio}
     */
    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    /**
     * Mètode per obtenir el valor de {@link #data}
     * 
     * @return Date S'obté un valor de tipus Date
     */
    public Date getData() {
        return data;
    }

    /**
     * Funció per canviar el contigut de {@link #data}
     * 
     * @param data Valor a actulitzar
     */
    public void setData(Date data) {
        this.data = data;
    }

    /**
     * Mètode per obtenir el {@link #tramHorari} de una oferta
     * 
     * @return String S'obté un de tipus String
     */
    public String getTramHorari() {
        return tramHorari;
    }

    /**
     * Amb aquesta funció canviará el valor d'un {@link #tramHorari} seleccionat
     * 
     * @param tramHorari Valor a actualitzar de tipus String
     */
    public void setTramHorari(String tramHorari) {
        this.tramHorari = tramHorari;
    }

    /**
     * Funció per obtenir la propietat de l'atribut {@link #preu}
     * 
     * @return double Ens retorna un valor del preu
     */
    public double getPreu() {
        return preu;
    }

    /**
     * Amb aquesta funció es canviara o actulitzarà el valor de {@link #preu}
     * 
     * @param preu El valor a actualitzar
     */
    public void setPreu(double preu) {
        this.preu = preu;
    }

}
