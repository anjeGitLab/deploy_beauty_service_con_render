package beauty.service.service.repositori.Entitats;


import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * <h1>Classe que representa una entitat de tipus Usuari </h1>
 * <p>Aquesta entitat gestionarà tipus d'usuaris amb dos rols predeterminats; "GESTOR" i "CLIENT".
 * A més aquesta classe té una relació Un a Molts amb la taula Reserves i Salo</p>
 * 
 */
@Entity
public class Usuari implements Serializable {
    private static final long serialVersionUID = 1L;



    @OneToMany(mappedBy = "usuari",fetch = FetchType.LAZY ,cascade = CascadeType.ALL)
    private Set<Reserves> reserves;
    
    @OneToMany(mappedBy = "usuari",fetch = FetchType.LAZY ,cascade = CascadeType.ALL)
    private Set<Salo> salo;

    @Id
    @NotNull
    @Column(name = "idUsuari")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Size(max=9)
    private int idUsuari;

    @NotNull
    @Column(name = "nom")
    @Size(max=20)
    private String nom;

    @NotNull
    @Column(name = "cognoms")
    @Size(max=45)
    private String cognoms;

    @Column(name="mail")
    @Size(max=45)
    @NotNull
    @Email
    private String mail;

    @NotNull
    @Column(name = "nomUsuari")
    @Size(max=20)
    private String nomUsuari;

    @Column(name="contrasenya")
    @Size(max=45)
    @NotNull
    private String contrasenya;


    @Column(name="ciutat")
    @Size(max=45)
    @NotNull
    private String ciutat;

    @Column(name="codiPostal")
    @Size(max=5)
    @NotNull
    private String codiPostal;


    @Column(name="rol")
    @NotNull
    private String rol;

  
    @Column(name = "active")
    @NotNull
    private Boolean active=true;


   /**
     * Constructor buit {@link Usuari} , que s'utilitzarà en la creació d'un objecte que serà l'instància d'una classe. Generalment
     * duu a terme les operacions requerides per inicialitzar la classe abans que els mètodes siguin invocats
     *  o s'accedeixi als metòdes
     */    
    public Usuari() {}
    
/**
 * Constructor per obtenir instacias de tipus {@link Usuari}
 * @param nom Paràmetre de tipus String, el qual se li assignarà un nom d'usuari.
 * @param cognoms Paràmetre per assignar els cognoms d'un usuari
 * @param mail Amb aquesta entrada, cada usuari tendra una adreça  electrònica
 * @param nomUsuari Valor per establir un user dins del sistema de reserves de servies.
 * @param contrasenya Valor secret que cada usuari tindrà
 * @param ciutat Ciutat de cada usuari
 * @param codiPostal Amb aquesta entra en servira per fer una reserva  perquè un usuari pugui triar un tipus de servei.
 * @param rol Els usuraris seran de dos tipus; "GESTOR" i "CLIENT"
 * @param active Amb aquest tindre dos valor 1 si un usuari està actiu i 0 si un usuari s'ha donat de baixa
 */
    public Usuari( String nom, String cognoms, String mail, String nomUsuari, String contrasenya, String ciutat, String codiPostal, String rol, Boolean active) {
        this.nom = nom;
        this.cognoms = cognoms;
        this.mail = mail;
        this.nomUsuari = nomUsuari;
        this.contrasenya = contrasenya;
        this.ciutat = ciutat;
        this.codiPostal = codiPostal;
        this.rol = rol;
        this.active = active;
    }


    
    /** 
     * S'estableix la relació entre Usuari i salo mitjançant  la clau forana salo
     * @return Set<Salo> Ens retorna un llistat de objectes tipus Salo.
     */
    public Set<Salo> getSalo() {
        return this.salo;
    }

    
    /**
     * S'actulitza la clua forna Salo  
     * @param salo Es passa per paràmetre una llista de tipus Salo.
     */
    public void setSalo(Set<Salo> salo) {
        this.salo = salo;
    }


    
    /** 
     * Amb aquests setter, s'obte la propietat de l'id usuari
     * @return int Ens tetorna un enter, que sera el valor d'id de l'usuari
     */
    public int getId() {
        return idUsuari;
    }



    
    /** 
     * Tot i que no és recomanable, se actualitza l'id principal de de taula Usuari, passant-li per 
     * paràmetre el valor a actualitzarzar
     * @param id
     */
    public void setId(int id) {
        this.idUsuari = id;
    }

    
    /** 
     * Amb aquesta funció s'obté el nom d'un usuari
     * @return String Ens retorna un objecte de tipus String, que serà el valor del nom d'un usuari.
     */
    public String getNom() {
        return nom;
    }

    
    /** 
     * Es fa servir aquesta funció per poder actualitzar el nom d'un usuari passant-li per paràmetre un valor de tipus String.
     * @param nom Valor de tipus String que serà el valor a actualitzar
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    
    /** 
     * Mètode por poder obtenir l'atribut {@link #cognoms} 
     * @return String Rertorna un valar de tipus String amb valor del {@link #cognoms}
     */
    public String getCognoms() {
        return cognoms;
    }

    
    /** 
     * La següent funció, guarda o actualitza el valor d'un {@link #cognoms}, passant-li per paràmetre un valor de tipus String, 
     * que serà el valor a canviar 
     * @param cognoms
     */
    public void setCognoms(String cognoms) {
        this.cognoms = cognoms;
    }

    
    /** 
     * Amb l'ajuda de la aquest mètode, s'obtendrà la propietat de l'atribut {@link #mail}
     * @return String Ens retorna un String, que sera el valor d'un objecte tipus mail
     */
    public String getMail() {
        return mail;
    }

    
    /** 
     * Es farà servir la següent funció per guardar o actualitzar el valor de l'atribut {@link #mail}, 
     passant-li per paràmetre un valor de tipus String que serà el valor a canviar
     * @param mail És el valor de la propietat del objecte mail.
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    
    /** 
     * Mètode per poder tenir accés a la propietat del atribut {@link #nomUsuari}
     * 
     * @return String Obtenint un valor de tipus String, que serà el nom d'un usuari
     */
    public String getNomUsuari() {
        return nomUsuari;
    }

    
    /**  
     * Aquesta funció per guardar o actualitzar el valor de l'atribut {@link #nomUsuari} 
     passant-li per paràmetre un valor de tipus String que serà el valor a canviar
     *  @param nomUsuari És el valor de la propietat del objecte {@link #nomUsuari} .
     *
     */
    public void setNomUsuari(String nomUsuari) {
        this.nomUsuari = nomUsuari;
    }

    
    /**
     * Mètode que farem servir per poder obtenir el valor de {@link #contrasenya} d'un {@link #Usuari()}
     * @return String El valor retornat sera un objecte de tipus String que serà el de la {@link #contrasenya} d'un usuari.
     */
    public String getContrasenya() {
        return contrasenya;
    }

    
    /** 
     * Aquest getter ens ajudarà a guardar o actualitzar el valor de l'atribut {@link #contrasenya}
     passant-li per paràmetre un valor de tipus String que serà el valor a canviar
     * @param contrasenya És el valor de la propietat del objecte {@link #contrasenya}
     */
    public void setContrasenya(String contrasenya) {
        this.contrasenya = contrasenya;
    }

    
    /** 
     *  Mètode obtenir el valor de {@link #ciutat} 
     * @return String Obtenim un valor de tipus String que ser el valor de l'atribut {@link #ciutat}
     */
    public String getCiutat() {
        return ciutat;
    }

    
    /**
     * Mètode per guardar o actualitzar el valor de l'atribut {@link #ciutat}
     passant-li per paràmetre un valor de tipus String que serà el valor a canviar 
     * @param ciutat  És el valor de la propietat del objecte {@link #contrasenya} a canviar
     */
    public void setCiutat(String ciutat) {
        this.ciutat = ciutat;
    }

    
    /** 
     * Amb la següent funció ens farà de suport per poder tenir accés a propietat de {@link #codiPostal}.
     * @return String S'obté el valor de {@link #codiPostal}
     */
    public String getCodiPostal() {
        return codiPostal;
    }

    
    /** 
     * Setter per poder actualitzar el valor de l'atribut {@link #codiPostal}
     passant-li per paràmetre un valor de tipus String que serà el valor a canviar 
     * @param codiPostal És el valor de la propietat del objecte {@link #codiPostal}, el qual serà el valor a canviar
     */
    public void setCodiPostal(String codiPostal) {
        this.codiPostal = codiPostal;
    }

    
    /**
     * Aquest mètode ens ajudarà per obtenir el valor de {@link #rol} d'un usuari. Els posibles valors seran: "GESTOR" i "CLIENT"
     * @return String El valor de {@link #rol}
     */
    public String getRol() {
        return rol;
    }

    
    /** 
     * Mètode per actualitzar el valor de l'atribut {@link #rol}
     passant-li per paràmetre un valor de tipus String que serà el valor a canviar 
     * @param rol Anb aquestets pàramtre, es canviara el valor d'un objecte tipus {@link #rol}
     */
    public void setRol(String rol) {
        this.rol = rol;
    }


    
    /** 
     * Funció per saber si un usuari s'ha donat de baixa o no a partir de una variable booleana
     * @return Boolean Depenent del valor de retorn sabre el seu estat.
     */
    public Boolean isActive() {
        return this.active;
    }

    
    /** 
     * Funció que ens ajudara a establir si un usuari s'ha donat de baixa o no, amb l'ajuda de una variable de tipus bolean
     passant-li per paràmetre un valor de tipus String que serà el valor a canviar 
     * @param active Valor per que serà actualizat segons l'estat d'un usuari
     */
    public void setActive(Boolean active) {
        this.active = active;
    }
    

    
    /** 
     * S'actulitza la clau forana reserves dins la taula {@link Usuari}
     * @param reserves Es passa per paràmtre llist de tipus {@link Reserves} a actualitzar
     */
    public void setReserves(Set<Reserves> reserves) {
        this.reserves = reserves;
    }

    
    /** 
     * Aquesta funció que estableix la relació amb la taula  {@link Reserves}
     * ens ajuda obtenir les dades a partir d'una llista de objectes de tipus {@link Reserves}
     * @return Set<Reserves> Ens retorna un llista amb objectes de tipus {@link Reserves}
     */
    public Set<Reserves> getReserves() {
        return this.reserves;
    }


    
    /** 
     * Mètode que ens diu l'estat d'un usuari. D'aquesta manera sobrem si un usuari s'ha donat de baixa o no
     * @return Boolean Depenent del resultat sabrem si un usuari se ha donat de baixa  o no.
     */
    public Boolean getActive() {
        return this.active;
    }



    
    /**
     * Función per poder tenir accés a la propietat d'un atribut {@link #idUsuari}
     * @return int Ens retorna una valor de tipus enter, amb el valor id {@link #idUsuari}
     */
    public int getIdUsuari() {
        return this.idUsuari;
    }

    
    /** 
     * Aquest setter ens ajuda a actualitzar el valor d'un atribut de tipus {@link #Usuari()} 
     passant-li per paràmetre un valor enter, que serà el valor a canviar.
     * @param idUsuari Aquest serà el valor a canviar.
     */
    public void setIdUsuari(int idUsuari) {
        this.idUsuari = idUsuari;
    }
}

