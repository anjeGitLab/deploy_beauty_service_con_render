package beauty.service.service.repositori.Entitats;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.persistence.GenerationType;

/**
 * <h1>Classe que representa una entitat {@link TipusServeis} <h1>
 * <p>Aquesta classe ens ajudarà a gestionar els tipus de servies que rebrà un usuari tipus "CLIENT"</p>
 */
@Entity
@Table(name="TipusServeis")
public class TipusServeis implements Serializable {
    private static final long serialVersionUID = 1L;


    @Id
    @NotNull
    @Column(name = "idTipusServei")
    @Size(max=11)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idTipusServei;

    @NotNull
    @Column(name = "nomTipusServei")
    @Size(min=1, max=45, message = "Has de ficar un tipus de servei")
    private String nomTipusServei;

    
    @NotNull
    @Column(name = "descripcioTipus")
    @Size(min=1, max=120, message = "Has de ficar  una descriptció")
    private String descripcioTipus;

    @ManyToOne(fetch = FetchType.LAZY ,cascade = CascadeType.ALL)
    @JoinColumn(name = "idSalo")
    private Salo salo;

/**
 * Sobre carrega de constructor, per obtenir una instancia únicament de {@link #nomTipusServei} i {@link #idTipusServei}
 * @param nomTipusServei Atribut {@link #nomTipusServei} de tipus String 
 * @param idTipusServei Atribut {@link #idTipusServei} de tipus enter
 */
    public TipusServeis(  String nomTipusServei,int idTipusServei) {
        this.nomTipusServei = nomTipusServei;
        this.idTipusServei = idTipusServei;
    }
    
    /**
     * Sobre carrega de constructor, per obtenir una instancia únicament de {@link #nomTipusServei} i {@link #descripcioTipus}
     * @param nomTipusServei Atribut {@link #nomTipusServei} de tipus String
     * @param descripcioTipus Atribut {@link #descripcioTipus} de tipus String
     */
    public TipusServeis( String nomTipusServei, String descripcioTipus) {
        this.nomTipusServei = nomTipusServei;
        this.descripcioTipus = descripcioTipus;
    }

    /**
     * Constructor buit {@link TipusServeis} , que s'utilitzarà en la creació d'un objecte que serà l'instància d'una classe. Generalment
     * duu a terme les operacions requerides per inicialitzar la classe abans que els mètodes siguin invocats
     *  o s'accedeixi als metòdes
     */
    public TipusServeis(){}


    
    /** 
     * Funció per obtenir la el valor de la propietat {@link #idTipusServei}
     * @return int Ens retorna un valor de tipus enter de l'atribut {@link #idTipusServei}
     */
    public int getIdTipusServei() {
        return idTipusServei;
    }


    
    /** 
     * Setter que ens servirà de suport per actualitzar el valor d'atribut {@link #idTipusServei}
     * @param idTipusServei Amb aquest valor de tipus enter, determinara el valor d'un nou valor de {@link #idTipusServei}
     */
    public void setIdTipusServei(int idTipusServei) {
        this.idTipusServei = idTipusServei;
    }

    
    /** 
     * Amb aquest mètode s’obte la propietat de l'atribut {@link #nomTipusServei}
     * @return String Aquesta funció en retorna el valor de l'atribut {@link #nomTipusServei}
     */
    public String getNomTipusServei() {
        return nomTipusServei;
    }

    
    /** 
     * Funció que actualitza la propietat de l'atribut  {@link #nomTipusServei}
     * @param nomTipusServei Amb aquest paràmetre podrem canviar el valor de {@link #nomTipusServei}
     */
    public void setNomTipusServei(String nomTipusServei) {
        this.nomTipusServei = nomTipusServei;
    }

    
    /** 
     * Amb aquest getter obtindrem el contingut de l'atribut {@link #descripcioTipus}, a més, aquest valor representa 
     * una característica o descripció de cada tipus de servei
     * @return String Ens retorna un valor de tipus String, el qual representa la propietat de l'atribut {@link #descripcioTipus}
     */
    public String getDescripcioTipus() {
        return descripcioTipus;
    }

    
    /** 
     * Mètode que canviarà el contingut de l'atribut {@link #descripcioTipus}
     * @param descripcioTipus Aquest paràmetre de tipus String, actualitzara el valor de {@link #idTipusServei}
     */
    public void setDescripcioTipus(String descripcioTipus) {
        this.descripcioTipus = descripcioTipus;
    }

    
}
