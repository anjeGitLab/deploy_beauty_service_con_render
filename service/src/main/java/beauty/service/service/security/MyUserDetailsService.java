package beauty.service.service.security;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import beauty.service.service.service.UsuariService;
import beauty.service.service.repositori.Entitats.Usuari;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Classe que implementa la interfície UserDetailsService i que comprova
 * si l'usuari existeix
 */
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UsuariService service;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuari usuari = service.getUsuariByUsername(username);
        
        if (usuari == null) {
           throw new UsernameNotFoundException("error");
        }

        return new MyUserDetails(usuari);
    }
    
}

