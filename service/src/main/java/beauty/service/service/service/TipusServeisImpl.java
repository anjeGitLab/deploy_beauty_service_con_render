package beauty.service.service.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import beauty.service.service.repositori.CrudRepositoriTipusServei;
import beauty.service.service.repositori.RepositoriTipusServeis;
import beauty.service.service.repositori.Entitats.Serveis;
import beauty.service.service.repositori.Entitats.TipusServeis;

/**
 * Classe que implementa mètodes per treballar amb els tipus serveis
 */
@Service
public class TipusServeisImpl implements TipusServeisService {

    @Autowired
    CrudRepositoriTipusServei crudRepositoriTipusServei;

    @Autowired
    RepositoriTipusServeis repositoriTipusServeis;

    /**
     * Funció que ens retorna una llista TipusServesis
     * 
     * @return Iterable<TipusServeis> Ens retorna uns llistat amb tots esl
     *         TipusServeis disponibles.
     */
    @Override
    public Iterable<TipusServeis> findAllServeis() {
        return crudRepositoriTipusServei.findAll();
    }

    /**
     * Mètode que pot o no retornar un llistar de TipusServies a partir d'un objecte TipusServeis
     * @param tipusServeis El TipusServeis a trobar
     * @return Optional<TipusServeis> Pot retornar o no un llistat de objectes TipusServeis.
     */
    @Override
    public Optional<TipusServeis> findByIdServeis(TipusServeis tipusServeis) {

        return crudRepositoriTipusServei.findById(tipusServeis.getIdTipusServei());
    }

    /**
     * Funció que guarda a la base de dades un objecte TipusServeis.
     * @param tipusServeis El tipus de objecte a guardar.
     * @return TipusServeis Retorna un objecte TipusServeis.
     */
    @Override
    public TipusServeis guardarServeis(TipusServeis tipusServeis) {
        return crudRepositoriTipusServei.save(tipusServeis);
    }

    /**
     * Mètode que esborra un objecte TipusServeis.
     * @param tipusServeis El objecte a esborrar
     */
    @Override
    public void deleteServeis(TipusServeis tipusServeis) {
        crudRepositoriTipusServei.save(tipusServeis);

    }

    /**
     * Funció que retorna tots el TipusServeis.
     * @return List<TipusServeis> Retorna un llistat de tots els serveis disponibles.
     */
    @Override
    public List<TipusServeis> findAllListTipusServeis() {

        return (List<TipusServeis>) crudRepositoriTipusServei.findAll();
    }

    /**
     * Funció que busca un TipusServeis a partir de un nom de TipusServeis donat.
     * @param nom Nom del TipuesServeis a trobar.
     * @return TipusServeis Llisat amb tots els TipusServeis a partir d'un nom.
     */
    @Override
    public TipusServeis findTipusServeiByNom(String nom) {
        return repositoriTipusServeis.findTipusServeiByNom(nom);
    }

    /**
     * Funció que ens retorna un llistat d'una serie numèrica a partir d'una llista de serveis
     * @param serveis Llistat de servies a gestionar.
     * @return List<Integer> Serie numèrica a partir un llistat de objectes tipus Serveis. 
     */
    @Override
    public List<Integer> llistatIdTipusServeis(List<Serveis> serveis) {
        return repositoriTipusServeis.llistatIdTipusServeis(serveis);
    }

    /**
     * Mètode que mostra un llistat de TipusServeis no triats per un usuari, a partir d'un array d'enters i un llistat  TipusServeis
     * @param serveisEscollits Array d'enters
     * @param allTipusServeis Array de tots els objectes TipusServeis.
     * @return List<TipusServeis> Ens retorna un llistat de tots els serveis no ecollits a partir d'un array d'enters i una llista de TipusServeis.
     */
    @Override
    public List<TipusServeis> llistatTipusServeisNoEscollits(List<Integer> serveisEscollits,
            List<TipusServeis> allTipusServeis) {
        return repositoriTipusServeis.llistatTipusServeisNoEscollits(serveisEscollits, allTipusServeis);
    }

    /**
     * Funció que retorna un llistat de tots els objectes TipusServeis.
     * @return List<TipusServeis> Ens dona un llistat tots els objectes TipusServeis.
     */
    @Override
    public List<TipusServeis> findTipusServeisAndId() {
        ArrayList<TipusServeis> listTipusServeisReturn = new ArrayList<>();
        List<TipusServeis> listTipusServeis = (List<TipusServeis>) crudRepositoriTipusServei.findAll();

        for (TipusServeis l : listTipusServeis) {
            listTipusServeisReturn.add(new TipusServeis(l.getNomTipusServei(), l.getIdTipusServei()));

        }
        return listTipusServeisReturn;
    }

    /**
     * Mètode que ens dona un objecte TipusServeis a partir de un id de TipusServeis.
     * @param idTipusServeis Identificador per poder trobar un objecte TipusServeis.
     * @return TipusServeis Retorna una objecte TipusServeis a partir de un id donat.
     */
    @Override
    public TipusServeis getTipusServeiById(int idTipusServeis) {
        return repositoriTipusServeis.getTipusServeiById(idTipusServeis);
    }

}
