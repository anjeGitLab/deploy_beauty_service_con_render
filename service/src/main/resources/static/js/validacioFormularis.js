// Validar editaGestorCentre.jsp i registreGestorCentre.jsp
/**
 * Función que passa a arrays elements inputs i els valida
 * @returns {String} - INPUT a validar
 */
 function validate() {
    var validateElements = document.getElementsByClassName("validate");
    var inputs = Array.prototype.filter.call(validateElements, function (element) {
        return element.nodeName === 'INPUT';
    });

    /**
     * Funció que valida un camp amb una expressió regular
     * @param {Boolean} email 
     * @returns {Boolean} 
     */
    const validateEmail = (email) => {
        return email.match(
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
    };

    for (var i = 0; i < inputs.length; i++) {
        var input = inputs[i];
        if (input.value.length == 0) {
            input.classList.add("error");
            input.focus();
            break;
        } else {
            input.classList.remove("error");
        }
        if (input.type == "number") {
            if (isNaN(input.value)) {
                alert("Introdueix un número");
                return false;
            }
        }
        if (input.type == "email") {
            if (!validateEmail(input.value)) {
                input.classList.add("error");
                input.focus();
            }
        }
    }

}

    

